#include "star.h"

Star::Star()
{
    size = qrand() % max_size;
    if (size < min_size) {
        size = min_size;
    }
    color = 0x00ffffff;
    color -= qrand() % 50 << 8*(qrand() % 4);
}

void Star::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{

    painter->setBrush(QBrush(color));
    painter->drawEllipse(QRect(0, 0, size, size));
}

QRectF Star::boundingRect() const
{
    QRectF rect(
        0,
        0,
        size,
        size
    );
    return rect;
}
