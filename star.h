#ifndef STAR_H
#define STAR_H
#include <QGraphicsObject>
#include <QPainter>
#include <QRgb>


class Star : public QGraphicsObject
{
public:
    Star();
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
private:
    static const int max_size = 5;
    static const int min_size = 2;
    int size;
    QRgb color;
};

#endif // STAR_H
