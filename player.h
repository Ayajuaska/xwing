#ifndef PLAYER_H
#define PLAYER_H
#include <QGraphicsItem>
#include <QGraphicsObject>
#include <QPainter>

class Player : public QGraphicsObject
{
    Q_OBJECT
public:
    Player(QGraphicsItem *parent=0);
    void init(const QPoint &start);
    QRectF boundingRect() const;
protected:
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
private:
    QImage *sprite;
};

#endif // PLAYER_H
