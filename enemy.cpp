#include "enemy.h"

Enemy::Enemy()
{
    sprite = new QImage("://images/tie_fighter.png");
}

void Enemy::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->drawImage(QPoint(0,0), *sprite);
}

QRectF Enemy::boundingRect() const
{
    QRectF rect(
        0, //this->pos().rx(),
        0, //this->pos().ry(),
        sprite->width(),
        sprite->height()
    );
    return rect;
}
