#ifndef PROJECTILE_H
#define PROJECTILE_H
#include <QGraphicsObject>
#include <QPainter>
#include <QRectF>

class Projectile : public QGraphicsObject
{
public:
    Projectile();
    QRectF boundingRect() const;
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
};

#endif // PROJECTILE_H
