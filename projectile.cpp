#include "projectile.h"

Projectile::Projectile()
{

}


void Projectile::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(QPen(QBrush(Qt::yellow), 2, Qt::SolidLine));
    painter->drawLine(QLine(QPoint(0, 0), QPoint(0, 20)));
}


QRectF Projectile::boundingRect() const {
    return QRectF(0, 0, 20, 2);
}
