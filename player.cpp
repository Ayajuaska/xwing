#include "player.h"

Player::Player(QGraphicsItem *parent) :
    QGraphicsObject(parent)
{

    sprite = new QImage(":/images/xwing.png");
}

void Player::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->drawImage(0, 0, *sprite);
}

void Player::init(const QPoint &start) {
    this->setPos(mapToParent(start));
}

QRectF Player::boundingRect() const {
    return QRectF(0, 0, sprite->width(), sprite->height());
}
