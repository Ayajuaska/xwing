#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <array>
#include <list>
#include <QDebug>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QLine>
#include <QList>
#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>
#include "enemy.h"
#include "player.h"
#include "projectile.h"
#include "star.h"

enum Movement {
    NO_MOVEMENT,
    LEFT,
    RIGHT
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void tick();
    bool eventFilter(QObject *object, QEvent *e);

signals:
    void score_change(int modifier);

protected:
    void make_shot();
    void init_stars();

private:
    QList<Star *> stars;
    std::list<Enemy *> enemies;
    std::list<Projectile *> projectiles;
    Player *player;
    QGraphicsScene *scene;
    QTimer* clk;
    QTimer* shot_cooldown;
    bool can_shoot;
    Ui::MainWindow *ui;
    Movement move_direction;
    int score;
};

#endif // MAINWINDOW_H
