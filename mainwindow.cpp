#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->resize(900, 720);
    ui->setupUi(this);
    scene = new QGraphicsScene(ui->graphicsView);
    scene->setBackgroundBrush(QBrush(QColor(Qt::black), Qt::SolidPattern));

    clk = new QTimer(this);
    shot_cooldown = new QTimer(this);
    can_shoot = true;
    player = new Player();
    for (auto i = 0; i < 3; i++) {
    auto enemy = new Enemy;
        enemy->setPos(qrand() % 700, 0);
        enemies.push_back(enemy);
        scene->addItem(enemy);
    }
    scene->addItem(player);
    ui->graphicsView->setScene(scene);
    init_stars();
    player->init(QPoint(350,500));
    connect(clk, &QTimer::timeout, this, &MainWindow::tick);
    connect(shot_cooldown, &QTimer::timeout, [this]() { this->can_shoot = true; });

    connect(this, &MainWindow::score_change, [this] (int modifier) { this->score += modifier; this->ui->lcdNumber->display(this->score); });
    clk->start(16);
    shot_cooldown->start(500);
}

bool MainWindow::eventFilter(QObject *, QEvent *e)
{
    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(e);
    if (e->type() == QEvent::KeyPress) {
        switch (keyEvent->key()) {
            case 0x1000012:
                move_direction = LEFT;
                break;
            case 0x1000014:
                move_direction = RIGHT;
                break;
            case 0x20:
                make_shot();
                break;
            default:
//                qDebug() << keyEvent->key();
                break;
        }
    }
    if (e->type() == QEvent::KeyRelease) {
        move_direction = NO_MOVEMENT;
    }
    return false;
}

void MainWindow::init_stars()
{
    Star *star;
    for (int i = 0; i < 200; i++) {
        qsrand(qrand());
        star = new Star();
        stars.append(star);
        scene->addItem(star);
        star->setPos(qrand() % 799, qrand() % 599);
    }
}

void MainWindow::make_shot()
{
    if (can_shoot) {
        if (projectiles.size() <= 10) {
            Projectile *shot = new Projectile;
            shot->setPos(
                player->pos().rx() + player->boundingRect().width() / 2,
                player->pos().ry()
            );
            projectiles.push_back(shot);
            scene->addItem(shot);
            can_shoot = false;
        }
    }
}

void MainWindow::tick()
{
    for (auto i = 0; i < stars.length(); i++) {
        Star *star = stars.at(i);
        qreal move_by = i % 4 + (0.1);
        star->moveBy(0, move_by * 2);
        if (star->pos().ry() >= 595) {
            star->setPos(qrand() % 790, 0);
        }
    }
    Projectile *to_delete = nullptr;
    for(auto shot : projectiles) {
        shot->moveBy(0, -3);
        for(auto enemy : enemies) {
            if (enemy->collidesWithItem(shot)) {
                enemy->setPos(qrand() % 700, -1 * qrand() % 100);
                shot->setPos(0, -1 * qrand());
                emit score_change(1);
            }
        }

        if (shot->pos().ry() <= 0) {
            to_delete = shot;
            scene->removeItem(shot);
        }
    }
    projectiles.remove(to_delete);
    to_delete->deleteLater();

    char dir = 0;
    for (auto enemy : enemies) {
        if (enemy->pos().rx() <= 0 || enemy->pos().rx() + enemy->boundingRect().width() >= 800) {
            dir *= -1;
        }
        enemy->moveBy(dir * (qrand() % 5 + 1), 1);
        if ((enemy->pos().ry() + enemy->boundingRect().height()) == player->pos().ry()) {
            if (enemy->pos().rx() + enemy->boundingRect().width() >= player->pos().rx() &&
                    enemy->pos().rx() <=player->pos().rx() + player->boundingRect().width()) {
                clk->stop();
                QMessageBox *mb = new QMessageBox(this);
                mb->setText(QString("You have crahed with enemy at (%1, %2)").arg(enemy->pos().rx()).arg(enemy->pos().ry()));
                qDebug() << QString("You have crahed with enemy at (%1, %2)").arg(enemy->pos().rx()).arg(enemy->pos().ry()) << "\n";
                qDebug() << QString("You were at (%1, %2)").arg(player->pos().rx()).arg(player->pos().ry()) << "\n";
                mb->show();
                return;
            }
        }
        if (enemy->pos().ry() + enemy->boundingRect().height() >= 600) {
             enemy->setPos(qrand() % 800, 0);
             score_change(-1);
        }
    }

    switch (move_direction) {
        case LEFT:
            player->moveBy(-5, 0);
            break;
        case RIGHT:
            player->moveBy(5, 0);
            break;
        default:
            break;
    }
    if (player->pos().rx() <= 0) {
        player->setPos(0, player->pos().ry());
    }
    if (player->pos().rx() >= 695) {
        player->setPos(695, player->pos().ry());
    }
    scene->update(0, 0, 800, 600);
}

MainWindow::~MainWindow()
{
    delete ui;
}
