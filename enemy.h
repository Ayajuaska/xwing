#ifndef ENEMY_H
#define ENEMY_H
#include <QGraphicsObject>
#include <QPainter>

class Enemy : public QGraphicsObject
{
public:
    Enemy();
    QRectF boundingRect() const;
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
private:
    QImage *sprite;
};

#endif // ENEMY_H
